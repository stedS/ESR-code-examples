/**
 * @file main.c
 * @brief Main File of Project ESR04-N07-Grace-LEDswitch
 * @author S. Steddin
 * @date 12.04.2018
 */

/** \mainpage ESR04-N07-Grace-LEDswitch
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie sich durch das Dr�cken der Taste S2
 *  des MSP430G2 Launchpads die gr�ne und die rote LED wechselseitig an- und
 *  ausschalten lassen. Der Programmentwurf erfolgt unter der Verwendung der
 *  GUI von TI-Grace.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S2 werden die gr�ne und die rote LED wechselseitig
 *     an- und ausgeschaltet.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 *  -# Bei Verwendung von Grace werden eine Reihe zus�tzlicher Konfigurations-
 *     und Source-Dateien erzeugt, die ebenfalls unter Versionskontrolle zu stellen sind.
 *  -# Die Nutzung der von TI-Grace generierten Dateien erfolgt unter der Anwendung der
 *     von TI vorgegebenen Nutzerlizenz (s.u.)
 *  -# Da im Code keine Interrupts und sleep modes verwendet werden, l�uft das Programm
 *     unter 100% Prozessorlast und hat daher eine sehr ung�nstige Energieaufnahme.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3         P1.0|-->LED red
 *       |    |             P1.6|-->LED green
 *      _|_
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Der Zustand von S2 wird von P1.3 eingelesen. Sobald der Pegel an P1.3 auf L
 *  geht, muss die Taste S2 gedr�ckt worden sein. Es muss daher solange gewartet werden
 *  bis der Pegel != H wird. Danach werden bei LEDs getoggelt. Anschlie�end muss gewartet
 *  werden, bis der Taster wieder losgelassen wird, also der Pegel von L wieder auf H
 *  zur�ckkehrt. \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */

/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/

/*
 * ======== Standard MSP430 includes ========
 */
#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

/*
 *  ======== main ========
 */
int main(void)
{
     Grace_init();                   // Activate Grace-generated configuration
    
	for(;;) {
		while ((P1IN & BIT3) == BIT3);	// wait until S2 is pressed
		P1OUT ^= BIT0 + BIT6;			// toggle green and red LED
		while ((P1IN & BIT3) == 0);		// wait until S2 is released
	}

    
    return (0);
}
