/***************************************************************************************
 *  ESR09-A01-AmE
 *  ----------------------------------------------------------
 *  Hardware: 		TI launchpad Rev. 1.5 mit MSP430G2553
 *  Software:		Code Composer Studio v6.1.2
 *  Beschreibung: 	Launchpad gibt die Melodie alle meine Entchen aus in C-Dur, beginnend
 *  				beim c' bis zum c'' aus. Das Dr�cken des Schalters S2 soll die Melodie
 *  				neu starten. Nach einmaligem Abspielen wartet das Programm auf den erneuten
 *  				Tastendruck auf S2. S2 ist entprellt. Das System wird vom DCO mit 1 MHz getaktet.
 *
 * Beschr�nkungen durch Aufgabenstellung: keine
 *
 *
 *                    MSP4302553
 *                  -----------------
 *                 |                 |
 *            S1-->|RST              |
 *                 |                 |
 *            S2-->|P1.3         P1.2|---|
 *                 |				 |	Lautsprecher
 *  			   |             GND |<--|
 *
 * S2 l�st beim Dr�cken einen Interrupt aus. Innerhalb der Interrupt-Routine
 * wird die Melodie neu gestartet. Die Erzeugung der T�ne erfolgt als PWM Signal,
 * welches von Timer0_A3 generiert wird. Die Ablaufsteuerung der T�ne erfolgt �ber
 * Timer1_A3. Die Melodie ist in einer Datenstruktur gespeichert, die sequentiell
 * durchlaufen wird.
 *
 * Vergleichswerte von Compare Register 1 von Timer0_A3 werden vorberechnet
 * und in einer Lookup-Table abgelegt.
 *
 * Weitere Besonderheit: die in der ISR von Taste S2 auszuf�hrenden Programmschritte
 * werden in einer Funktion in main.c behandelt. Auf diese Weise bleibt der von Grace
 * erzeugte Code von InterruptVectors.c unver�ndert und die selbst erstellten
 * Programmfunktionen finden sich an einer Stelle (main.c)
 *
 * Verfahren l�uft ohne CPU: Low Power Mode 1
 *
 *  Prof. Dr. S. Steddin
 *  HS-Reutlingen - Med. Technische Informatik
 */
#include <msp430.h>
#include <stdint.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

//Umrechnung der Tonleiterfrequenzen in Timerz�hlerst�nde,
//ausgehend von a' (440Hz):
// c'' = h' * 2^(1/12) --> 1910
// h'  = a' * 2^(2/12) --> 2024
// a'  = 440 Hz        --> 2272
// g'  = a' / 2^(2/12) --> 2550
// f'  = g' / 2^(2/12) --> 2862
// e'  = f' / 2^(1/12) --> 3032
// d'  = e' / 2^(2/12) --> 3404
// c'  = d' / 2^(2/12) --> 3821
//Beachten: die Periode (Z�hlerwert) ist reziprok zur Frequenz!

//Indexwert f�r Zugriff auf Tonleiter
typedef enum Enum_Ton { c1=0, d1, e1, f1, g1, a1, h1, c2} tEnum_Ton;

//Definition der Z�hlerwerte f�r die PWM Frequenz zur Ausgabe der T�ne
const unsigned int cTonleiter[] = { 3821, 3404, 3032, 2862, 2550, 2272, 2024, 1910};

const uint16_t d8_25ms = 8;		//60s / (80 beats/s) / 25ms
typedef enum Enum_Dauer {t8=1, t4=2, t2=4, t1=8} tEnum_Dauer;


typedef struct					//Datentyp einer Note
{
	tEnum_Dauer eDauer;
	tEnum_Ton eTon;
} tStruct_Note;

//Definition des Liedes
const tStruct_Note cArr_AmE[] = {
	{t8,c1},{t8,d1},{t8,e1},{t8,f1},{t4,g1},{t4,g1},
	{t8,a1},{t8,a1},{t8,a1},{t8,a1},{t2,g1},
	{t8,a1},{t8,a1},{t8,a1},{t8,a1},{t2,g1},
	{t8,f1},{t8,f1},{t8,f1},{t8,f1},{t4,e1},{t4,e1},
	{t8,d1},{t8,d1},{t8,d1},{t8,d1},{t2,c1}
};

uint16_t uiNoteIdx = 0;				//Index der aktuellen Note
uint16_t uiActRestDauer_25ms;		//verbleibenden Spieldauer des aktuellen Tons
int16_t  i_EntprellZaehler = 0;		//Wartezyklen der ISR nach Tastendruck

/*
 *  ======== main ========
 */
int main(void)
{
    Grace_init();                   //Activate Grace-generated configuration
	TA1CTL |= MC_0;					//Sequencer Z�hler stoppen, damit keine Interrupts
									//ausgel�st werden (k�nnte auch �ber TAIE erfolgen)
	TA0CCTL1 &= ~OUTMOD_7;			//Tonausgabe deaktivieren

    return (0);
}



void S2_ISR(void)
{
	volatile unsigned int ii = 500;

	//Entprellen der Taste S2:
	//a) Warten bis die Taste sich beruhigt hat:

	//	while (ii--) _nop();
	//Warteschleife (Resourcenverschwendung / schlechtes Design!)

	//Alternative Entwprellung unter Nutzung des bereits vorhandenen
	//25 ms Timers:
	//Der Tasten-ISR deaktiviert den Tasteninterrupt und der 25 ms Timer
	//schaltet den Interrupt wieder ein, sobald mindestens 25 ms vergangen sind.

	i_EntprellZaehler = 2;  //25ms Timer muss mindestens 2 mal aufgerufen werden,
							//bevor der Tasteninterrupt wieder aktiviert wird; es
							//ergibt sich somit eine Periode zwischen 25 ... 50 ms
							//w�hrend welcher der Interrupt nicht reagiert.
	P1IE &= ~BIT3;			//Interrupt ausschalten

	//b) Nur dann reagieren, wenn die Taste gedr�ckt ist (L-Pegel):
	if (!(P1IN & BIT3))
	{
		uiNoteIdx = 0;
		uiActRestDauer_25ms = 0; 		//Abholung der n�chsten Note ausl�sen
		TA1R = 24999;					//Counter so setzen, dass unmittelbar
										//nach dem n�chsten Z�hltakt die Seq_ISR
										//aufgerufen wird
		TA1CTL |= MC_1;					//Z�hler in Up Mode setzen, also starten
	}
	P1IFG &= ~BIT3;						//Interruptflag von Taste S2 zur�ck setzen
}

void Seq_ISR(void)
{
	TA1CTL &= ~TAIFG;					//Interruptflag des Seq-Timers zur�cksetzen

	if (uiActRestDauer_25ms == 0)		//wenn Note komplett abgespielt --> n�chste Note holen
	{
		if (uiNoteIdx >= 27)
		{
			TA1CTL &= ~MC0;				//wenn Lied fertig: Z�hler des Seq-Timer anhalten
										//somit werden vom Timer keine weiteren Interrupts ausgel�st
		}
		else
		{
			//n�chsten Ton einrichten:
			uiActRestDauer_25ms = cArr_AmE[uiNoteIdx].eDauer * d8_25ms; // Berechnung der Dauer in TimerTicks
			TA0CCR0 = cTonleiter[cArr_AmE[uiNoteIdx].eTon]; 			// >> 4 (4 Oktaven h�her);
			TA0CCR1 = TA0CCR0 >>1;										// Definition des 50% Tastverh�ltnisses
			uiNoteIdx++;												// n�chste Note adressieren
			TA0CCTL1 |= OUTMOD_7;										// Tonausgabe wieder aktivieren
		}
		return;
	}
	if (uiActRestDauer_25ms == 1)
	{
		TA0CCTL1 &= ~OUTMOD_7;			// 25ms vor Ende des Tons Tonausgabe abschalten
										// um Trennung der Einzelt�ne zu erm�glichen
	}
	uiActRestDauer_25ms--;				// verbleibende Dauer dekrementieren


	//Entprellung der Taste S2:
	if (i_EntprellZaehler >= 0)
	{
		if (i_EntprellZaehler == 0)
		{
			P1IE |= BIT3;				// Tasteninterrupt wieder einschalten
		}
		i_EntprellZaehler--;
	}
}
