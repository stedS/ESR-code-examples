/**
 * @file ESR04-N06a-ToggleRedGreenOnS2.c
 * @brief Main File of Project ESR04-N06a-ToggleRedGreenOnS2
 * @author S. Steddin
 * @date 02.04.2016
 */

/** \mainpage ESR04-N06a-ToggleRedGreenOnS2
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie sich durch das Dr�cken der Taste S2
 *  des MSP430G2 Launchpads die gr�ne und die rote LED wechselseitig an- und
 *  ausschalten lassen.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S2 werden die gr�ne und die rote LED wechselseitig
 *     an- und ausgeschaltet.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3         P1.0|-->LED red
 *       |    |             P1.6|-->LED green
 *      _|_
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Der Zustand von S2 wird von P1.3 eingelesen. Sobald der Pegel an P1.3 auf L
 *  geht, muss die Taste S2 gedr�ckt worden sein. Es muss daher solange gewartet werden
 *  bis der Pegel != H wird. Danach werden bei LEDs getoggelt. Anschlie�end muss gewartet
 *  werden, bis der Taster wieder losgelassen wird, also der Pegel von L wieder auf H
 *  zur�ckkehrt. \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */
#include <msp430.h>

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= BIT0 + BIT6;			// Set P1.0 and P1.6 to output direction
									// Any other ports are set to input direction, as all
									// other bit positions are set to 0 by default
	P1REN |= BIT3;					// enable internal Resistor
	P1OUT |= BIT3;					// configure internal Resistor as pullup Resistor

	P1OUT |= BIT0;					// enable red LED
	P1OUT &= ~BIT6;					// disable green LED

	for(;;) {
		while ((P1IN & BIT3) == BIT3);	// wait until S2 is pressed
		P1OUT ^= BIT0 + BIT6;			// toggle green and red LED
		while ((P1IN & BIT3) == 0);		// wait until S2 is released
	}

	return 0;
}



