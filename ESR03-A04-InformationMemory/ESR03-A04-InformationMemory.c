/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************
//  MSP430G2xx3 Demo - Flash user name to SegC of information memory
//
//  Description: This program first erases flash seg C, then it writes the
//  name of the user at start-address of SegC: LastnameFirstName (e.g. SchmidFritz).
//  Finally it is checked whether the content of the Flash memory is equivalent to
//  the user's name (green LED on; otherwise red LED on).
//  Assumed MCLK 771kHz - 1428kHz.
//
//               MSP430G2xx3
//            -----------------
//        /|\|              XIN|-
//         | |                 |
//         --|RST          XOUT|-
//           |                 |
//
//  This code is derived from TI sample code project msp430g2xx3_flashwrite_01
//  Built with CCS Version 6.1.1
//******************************************************************************

/**
 * @file ESR03-A04-InformationMemory.c
 * @brief Main File of Project ESR03-A04-InformationMemory
 * @author S. Steddin
 * @date 08.04.2018
 */

/** \mainpage ESR03-A04-InformationMemory
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie der Inhalt des Seg C des
 *  Inforamtion Memory mit eigenen Inhalten gef�llt werden kann: Es wird
 *  der eigene Name an den Beginn des Segments geschrieben und anschlie�end
 *  kontrolliert, ob der Inhalt des Seg C auch wirklich den vorgegebenen Namen
 *  enth�lt. Ist der Vergleich ok, so soll die gr�ne LED leuchten, andernfalls
 *  soll die rote LED leuchten.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Die Applikation l�uft, ohne durch Eingaben beeinflusst werden zu k�nnen.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Es muss sichergestellt werden, dass der zu schreibende String die Gr��e
 *     des Speichersegments von SegC nicht �berschreitet.
 * \subsection remarks Hinweise
 *  -# Beim Schreiben in den Information memory m�ssen bestimmte Steuerflags gesetzt
 *     werden. Der Code hierf�r wird von dem Beispielcode aus Projekt msp430g2xx3_flashwrite_01
 *     von TI �bernommen. Die im Sourcecode hinterlegten Copyright Hinweise sind zu beachten.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *            |             P1.0|-->LED red
 *            |             P1.6|-->LED green
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Zuerst wird ein String mit einem Namen in das Seg C des Information memory
 *  geschrieben und anschlie�end wird der Inhalt des Seg C mit dem zu schreibenden
 *  Namen verglichen. Stimmt der Name mit dem Inhalt des Information memory �berein,
 *  so leuchtet die gr�ne LED andernfalls die rote LED.
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */

#include <msp430.h>
#include <string.h>

const unsigned int SEGCADDR = 0x1040;
const char myName[] = "SteddinSven";

// Function prototypes
void write_String2SegC(const char* string);

int main(void) {
	WDTCTL = WDTPW + WDTHOLD;               // Stop watchdog timer
	if (CALBC1_1MHZ == 0xFF) {				// If calibration constant erased
		while (1)
			;                               // do not load, trap CPU!!
	}
	DCOCTL = 0;                          	// Select lowest DCOx and MODx settings
	BCSCTL1 = CALBC1_1MHZ;                  // Set DCO to 1MHz
	DCOCTL = CALDCO_1MHZ;
	FCTL2 = FWKEY + FSSEL0 + FN1;           // MCLK/3 for Flash Timing Generator

	P1DIR = BIT0 | BIT6;

	write_String2SegC(myName);            	// Write string to segment C

	while (1)                               // Repeat forever
	{
		if (strcmp(myName, (char*) SEGCADDR) != 0) {
			P1OUT = BIT0;
		} else {
			P1OUT = BIT6;
		}
	}
}


void write_String2SegC(const char* string) {
	char *Flash_ptr = (char*) SEGCADDR;		// Start address of inf-mem seg C
	char *source = (char*)(void*)string;	// Start address of string to be copied
											// Type cast required in order to assign const char* to char*
	unsigned int cnt = 64;					// copy counter: size of inf-mem segment

	FCTL1 = FWKEY + ERASE;                  // Set Erase bit
	FCTL3 = FWKEY;                          // Clear Lock bit
	*Flash_ptr = 0;                        	// Dummy write to erase Flash segment

	FCTL1 = FWKEY + WRT;                    // Set WRT bit for write operation

	while (*source != 0) {
		*Flash_ptr++ = *source++;           // Write value to flash
		if (--cnt == 0)
			break;							// Stop writing, if string exceeds size of inf-mem
	}
	*Flash_ptr = 0;							// String end value : '0'
	FCTL1 = FWKEY;                          // Clear WRT bit
	FCTL3 = FWKEY + LOCK;                   // Set LOCK bit
}

