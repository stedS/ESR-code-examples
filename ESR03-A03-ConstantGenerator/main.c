/**
 * @file main.c
 * @brief Main File of Project ESR03-A03-ConstantGenerator
 * @author S. Steddin
 * @date 08.04.2018
 */

/** \mainpage ESR03-A03-ConstantGenerator
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, dass sich bei der Verwendung
 *  des Konstantengenerators des MSP430 ein Geschwindigkeitsvorteil ergibt.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Die Applikation l�uft, ohne durch Eingaben beeinflusst werden zu k�nnen.
 *  -# Wenn die Addition +2 (Konstantengenerator) tats�chlich schneller durchgef�hrt
 *     wird als die Addition von +3, so sollte sich beim Blinken der roten LED ein
 *     Tastverh�ltnis != 50% ergeben
 *  -# Um zu erkennen, wie der Code tats�chlich ausgef�hrt wird, ist es erforderlich
 *     den Disassembly View u �ffnen und anzuschauen, wie der generierte Assemblercode
 *     aussieht. Gleichzeitig sollte auch noch mit den Optimierungseinstellungen des
 *     Compilers experimentiert werden, um zu sehen, welches Einfluss dies auf die
 *     Ausf�hrung des Programmes hat.
 *  -# Die Dauer des Blinkens der LED kann entweder mit einem Oszilloskop beobachtet oder
 *     �ber eine Stoppuhr gemessen werden (hierzu muss dann allerdings eine unsigned long
 *     Variable verwendet und die Anzahl der Schleifendurchl�ufe erh�ht werden, bzw. es
 *     m�ssen 2 Schleifen ineinander verschachtelt werden, wenn mit unsigned int gearbeitet
 *     werden soll.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Die Z�hlervariablen als volatile deklarieren, wenn diese f�r das Debugging
 *     ben�tigt wird.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *            |             P1.0|-->LED
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Toggle P1.0 by xor'ing P1.0 inside of a software loop. \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */

#include <msp430.h> 


int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    P1DIR = BIT0;
    P1OUT = 0x00;

    volatile unsigned int sum1;
    volatile unsigned int sum2;


    while (1) {
		sum1 = 0;
    	while (sum1 < 0x10000) {
			sum1 +=2;
		}
    	sum2 = 0;
		P1OUT |= BIT0;
		while (sum2 < 15000) {
			sum2 +=3;
		}
		P1OUT &= ~BIT0;
    }
	return 0;
}
