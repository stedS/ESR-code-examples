/***************************************************************************************
 *  ESR09-N01-Stimmgabel
 *  ----------------------------------------------------------
 *  Hardware: 		TI launchpad Rev. 1.5 mit MSP430G2553
 *  Software:		Code Composer Studio v6.1
 *  Beschreibung: 	Launchpad soll 440 Hz-Ton ausgeben
 *
 * Beschr�nkungen durch Aufgabenstellung: keine
 *
 *
 *                    MSP4302553
 *                  -----------------
 *                 |                 |
 *            S1-->|RST              |
 *                 |                 |
 *                 |             P1.2|---|
 *                 |				 |	Lautsprecher
 *  			   |             GND |<--|
 *
 *
 * 1.  Timerblock: Timer0_A3
 * 2.  Taktfrequenz : 1 MHz DCO, kalibriert
 * 3.  Timermode: Up Mode
 * 4.  Timer Outputmode: Set / Reset
 * 5.  Definition der Controllregister:
 * 5.1 TA0CTL0 : Timer_A Control
 * 5.2 TA0CCTL0 : Timer_A0 Capture/Compare Control
 * 5.3 TA0CCR0 : Timer_A0 Capture/Compare 0 --> Grundfrequenz
 * 5.4 TA0CCR1 : Timer_A0 Capture/Compare 0 --> PWM-Toggle
 * 6.  P1DIR : Output-Pin festlegen
 * 7.  P1SEL : Outputpin mit TimerA0 CCR1 verbinden
 * 8.  LowPowerMode w�hlen
 *
 * Verfahren l�uft ohne Interrupt und ohne CPU: Low Power Mode 1
 *
 * Erweiterung:
 * Wird der #define WOBBEL deklariert, dann wird der Lautsprecher in einer Schleife mit abnehmend
 * tieferen Frequenzen angesteuert. Die Abnahme der Frequenz erfolgt mit einer Schrittweite, die
 * zur jeweils aktuellen Frequenz proportional ist.
 *
 * Das Programm wird ebenfalls mit GRACE erstellt, um die unterschiedliche
 * Form der Programmierung zu demonstrieren.
 *
 *  Prof. Dr. S. Steddin
 *  HS-Reutlingen - Med. Technische Informatik
 */

#include <msp430.h> 
#include <stdint.h>

/*
 * main.c
 */
int main(void)
{
    //Watchdog lahmlegen:
    WDTCTL = WDTPW | WDTHOLD;

    //Einstellung des DCO auf 1 MHz anhand der
    //vorkalibrierten Werte aus dem Information Memory

    if (CALBC1_1MHZ != 0xFF)
    {
        DCOCTL = 0x00;
        BCSCTL1 = CALBC1_1MHZ;      /* Set DCO to 1MHz */
        DCOCTL = CALDCO_1MHZ;
    }

    //Einstellung der Clock-Source SMCLK
    //Beachte: Peripherie kann nicht �ber den MCLK angesteuert werden -->
    //andere Clock-Source verwenden: SMCLK, ACLK, VLOCLK, ...
    //Hier: Verwendung von SMCLK f�r TimerA3_0

    BCSCTL2 = SELM_0 + DIVM_0 + DIVS_0;
    //0x0000 = 0b 0000 0000 0000 0000

    //Festlegung von der Kontrollregister von TimerA3_0:

    //Zuweisung der Taktquelle f�r SMCLK, des Vorteilers und des
    //Counter mode (up mode)
    TA0CTL = TASSEL_2 + ID_0 + MC_1;
    //0x0210 = 0b 0000 0010 0001 0000

    //Festlegung Capture mode (inaktiv) und Output mode
    TA0CCTL1 = CM_0 + CCIS_0 + OUTMOD_7;
    //0x00E0 = 0b 0000 0000 1110 0000


    //Festlegung der Compare Register f�r Grundfrequenz (TA0CCR0)
    //und f�r Tastverh�ltnis von 50% (TACCR1)
    TA0CCR0 = 2272>>1;
    TA0CCR1 = 1136>>1;
    //Shift >>1 bewirkt Sprung um eine Oktave vom a' zum a''

    //An welchem Pin liegt der Ausgang des Compare-Block 1 von TimerA3_0?
    //Im Prozessor Datenblatt nachschlagen ... :
    //TAO.1 (TimerA Out von Block 1) wird dann auf Pin P1.2 gelegt, wenn
    //P1DIR und P1SEL gesetzt sind. Der Timer Ausgang ist nur auf diesem
    //Pin abgreifbar!
    P1DIR = BIT2; 	// 0000 0100
    P1SEL = BIT2;	// 0000 0100


//#define WOBBEL
#ifdef WOBBEL
    while (1)
    {
    	uint16_t uiDauer, uiPeriode, kk;
    	for (uiDauer = 50; uiDauer<40000; uiDauer += uiDauer>>1)			//Bestimmung der Geschwindigkeit mit der die Tonfolge durchlaufen wird
    	{
    		for (uiPeriode = 64; uiPeriode<8192; uiPeriode+= uiPeriode>>4) 	//Bestimmung der Frequenz des Tones
			{
				TA0CCR0 = uiPeriode;
				TA0CCR1 = uiPeriode>>1;
				kk = uiDauer;
				while (kk--) { _nop(); }
			}
    	}
    }
#endif


    // Aktivierung des Sleep modes:
    //Nach Einstellung der Register hat die CPU nichts mehr zu tun. Die Timer
    //laufen im passend gew�hlten sleep mode selbstst�ndig
    LPM1;

	return 0;

}
