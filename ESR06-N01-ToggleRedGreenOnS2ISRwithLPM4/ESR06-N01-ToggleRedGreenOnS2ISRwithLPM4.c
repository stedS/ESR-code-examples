/**
 * @file main.c
 * @brief Main File of Project ESR06-N01-ToggleRedGreenOnS2ISRwithLPM4
 * @author S. Steddin
 * @date 26.04.2016
 */

/** \mainpage ESR06-N01-ToggleRedGreenOnS2ISRwithLPM4
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie low poweer modes des Mikrocontrollers
 *  beim event-driven programming zum Einsatz kommen.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S2 werden die gr�ne und die rote LED wechselseitig
 *     an- und ausgeschaltet.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3         P1.0|-->LED red
 *       |    |             P1.6|-->LED green
 *      _|_
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Beim Dr�cken von S2 (fallende Flanke an P1.3) wird die zugeh�rige ISR gestartet.
 *  In der ISR erfolgt die Umschaltung der LEDs. Es ist wiederum erforderlich, eine
 *  Warteschleife einzubauen, �ber welche die Taste S2 entprellt wird, um zu verhindern,
 *  dass unmittelbar nach der ersten fallenden Flanke durch weitere fallende Flanken,
 *  die durch das Prellen der Taste zustande kommen, weitere Interrupts ausgel�st werden.
 *  Ebenso muss das Interrupt Flag von Eingang P1.3 gel�scht werden, da andernfalls
 *  unmittelbar nach dem Verlassen der ISR diese erneut aufgerufen w�rde.\n
 *  @image html stateDiagram.png "Aufbau des Programms" \n
 *  @image rtf stateDiagram.png width=15cm \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */


#include <msp430.h>
#include <stdint.h>

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= BIT0 + BIT6;			// Set P1.0 and P1.6 to output direction
									// Any other port pins are set to input direction, as all
									// other bit positions are 0 by default
	P1REN |= BIT3;					// enable internal resistor
	P1OUT |= BIT3;					// configure internal resistor as pullup resistor
	P1IE |=  BIT3;                  // P1.3 Taster S2: interrupt enabled
	P1IES |= BIT3;                  // P1.3 Taster S2: interrupt with hi/lo edge
	P1IFG &= ~BIT3;                 // P1.3 IFG cleared

	P1OUT |= BIT0;					// enable red LED
	P1OUT &= ~BIT6;					// disable green LED

    __bis_SR_register(GIE);         // Global interrupt enable
    _BIS_SR(LPM4_bits);				// Macro instead of intrinsic function: Enter LPM4

    //Warum fehlt hier die Endless Loop?

	return 0;
}

// Port 1 interrupt service routine
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	uint16_t ii = 0xFFFF;

	P1OUT ^= BIT0 + BIT6;					// toggle green and red LED
	while (ii--) _nop();					// debounce switch
	P1IFG &= ~BIT3;                         // P1.3 IFG cleared
}

