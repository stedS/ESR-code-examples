/**
 * @file main.c
 * @brief Main File of Project ESR08-N05-Grace-RedLEDOffAfter5Sec
 * @author S. Steddin
 * @date 12.04.2018
 */

/** \mainpage ESR08-N05-Grace-RedLEDOffAfter5Sec
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie der Compare Mode eines Timers dazu
 *  verwendet werden kann, nach Ablauf einer vorgegebenen Zeit einen Interrupt
 *  auszul�sen. Dieser dient in diesem Programm dazu, die rote LED auszuschalten.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken von S2 wird die rote LED eingeschaltet und der Timer0A1 gestartet.
 *  -# Nach 3 Sekunden l�st der Timer eine Interrupt aus und schaltet die LED wieder aus.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Die Clock Unit muss zu Beginn ber Grace so konfiguriert werden, dass der Z�hltakt
 *     so langsam ist, dass damit 3 Sekunden ohne �berlauf erfasst werden k�nnen.
 *  -# Die Nutzung der von TI bereitgestellten Informationen und Codeteile erfolgt unter
 *  der Anwendung der von TI vorgegebenen Nutzerlizenz (s.u.)

 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                       MSP430x5xx
 *                     -----------------
 *                 /|\|              XIN|-
 *         int.     | |                 |
 *         pullup   --|RST          XOUT|-
 *            |       |                 |
 *            --------|P1.3         P1.0|-->LED red
 *            |       |                 |
 *        	  \	S2
 *            |
 *           --- GND
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  -# Die Interruptfunktion f�r die fallende Flanke an S2 setzt das Counterregister
 *     von Timer0A1 zur�ck und startet den Timer dann im Up-Mode.
 *  -# Der Timer z�hlt, bis der auf 3 Sekunden eingestellt Z�hlerwert erreicht wird. In
 *     diesem Moment l�st der Z�hler einen Interrupt aus, in dessen ISR die LED wieder
 *     ausgeschaltet wird.
 * \subsection results Resultate
 *  @image html ScreenshotOsciLEDOnPeriod.png "Dauer der On-Periode der roten LED" \n
 *  @image rtf ScreenshotOsciLEDOnPeriod.png width=15cm \n*
 *
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */


/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/

/*
 * ======== Standard MSP430 includes ========
 */
#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

/*
 *  ======== main ========
 */
int main(void)
{
    Grace_init();                   // Activate Grace-generated configuration
    
    // >>>>> Fill-in user code here <<<<<
    
    return (0);
}
