/**
 * @file ESR06-N01b-RedLedOnInIsrOffInMain.c
 * @brief Main File of Project ESR06-N01b-RedLedOnInIsrOffInMain
 * @author S. Steddin
 * @date 23.04.2018
 */

/** \mainpage ESR06-N01b-RedLedOnInIsrOffInMain
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie zeitintensive Vorg�nge aus der ISR
 *  in die main loop zur�ckverlagert werden, um die Reaktionsf�higkeit eines
 *  Programmes aufrecht zu erhalten.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S2 wird die rote LED angeschaltet. Die LED leuchtet
 *     solange, wie die Taste S2 gedr�ckt bleibt.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3         P1.0|-->LED red
 *       |    |                 |
 *      _|_
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Das Programm befindet sich nach der Initialisierung der Konfigurationsregister
 *  im LPM4, kann also nur durch ein externes Event an einem DIO-Port geweckt werden.
 *  Beim Dr�cken von S2 (fallende Flanke an P1.3) wird die zugeh�rige ISR gestartet.
 *  In der ISR erfolgt das Anschalten der roten LED. Um nun nicht in der ISR verbleiben
 *  zu m�ssen, bis die Taste wieder losgelassen wird, gibt es zwei M�glichkeiten:
 *  -# in der ISR wird der Interrupt umprogrammiert, so dass er anschlie�end auf eine
 *     steigende Flanke reagiert (Taste loslassen). Dies darf nat�rlich erst geschehen,
 *     wenn sichergestellt ist, dass das Prellen der Taste abgeklungen ist.
 *  -# in der ISR wird die LED eingeschaltet und anschlie�end erfolgt ein R�cksprung in
 *     die main-loop, die nun die �berwachung des Zustands der Taste �bernimmt. Auf diese
 *     Weise bleibt das Programm f�r andere m�glicherweise auftretende Interrupts nutzbar.
 *  In diesem Beispiel wird L�sung 2 umgesetzt. Die ISR wird so kurz wie m�glich gestaltet,
 *  somit auch die Schleife f�r das Entprellen von S2 in die main-loop verlagert. Dies
 *  bedeutet nat�rlich, dass beim Prellen der Taste die ISR mehrfach aufgerufen werden kann.
 *  Dies hat auf die Funktion jedoch keinen Einfluss. Soll der mehrfache Aufruf der ISR
 *  nachgewiesen werden, kann in der ISR ein statischer Z�hler inkrementiert und im debug-
 *  Modus ausgelesen werden. \n
 *  @image html stateDiagram.png "Aufbau des Programms" \n
 *  @image rtf stateDiagram.png width=15cm \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */


#include <msp430.h>
#include <stdint.h>

#define DEBUGCNT					// decomment line to activate bounce inspection code

#ifdef DEBUGCNT
unsigned int bounceCnt = 0;
#endif

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= BIT0;					// Set P1.0 (red LED) to output direction
	P1OUT &= ~BIT0;					// switch red LED off
									// Any other port pins are set to input direction, as all
									// other bit positions are 0 by default
	P1REN |= BIT3;					// enable internal resistor for S2
	P1OUT |= BIT3;					// configure internal resistor as pullup resistor
	P1IE |=  BIT3;                  // P1.3 Taster S2: interrupt enabled
	P1IES |= BIT3;                  // P1.3 Taster S2: interrupt with hi/lo edge
	P1IFG &= ~BIT3;                 // P1.3 IFG cleared



    __bis_SR_register(GIE);         // Global interrupt enable
    _BIS_SR(LPM4_bits);				// Macro instead of intrinsic function: Enter LPM4

    for (;;) {
    	uint16_t ii = 0x3FFF;

    	while (ii--) _nop();					// debounce switch
    	while ((P1IN & BIT3) != BIT3);			// wait until S2 released
    	P1OUT &= ~BIT0;							// switch off red LED

#ifdef DEBUGCNT
    	if (bounceCnt > 20)						// compare bounceCnt with real count
    		bounceCnt = 0;						// place breakpoint here to check bouncing of S2
#endif
    	LPM4;									// start LPM 4
    }

	return 0;
}

// Port 1 interrupt service routine
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
#ifdef DEBUGCNT
	bounceCnt++;							// increment bouncing counter
#endif
	P1OUT |= BIT0;							// toggle green and red LED
	P1IFG &= ~BIT3;                         // P1.3 IFG cleared
	LPM4_EXIT;								// return without restoring LPM
}

