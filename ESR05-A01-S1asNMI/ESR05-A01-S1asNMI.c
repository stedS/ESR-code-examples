/**
 * @file main.c
 * @brief Main File of Project ESR05-A01-S1asNMI
 * @author S. Steddin
 * @date 12.04.2018
 */

/** \mainpage ESR05-A01-S1asNMI
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie sich auch Taste S1, die eigentlich mit
 *  der Reset-Leitung verbunden ist, in einem Programm als Eingabetaste nutzen l�sst.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S1 wird die gr�ne LED eingeschaltet, durch Dr�cken von S2
 *     wird die LED wieder ausgeschaltet
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 *  -# S1 ist bereits mit einem externen Pullup-Widerstand versehen
 *  -# Wird der NMI verwendet, so steht die Reset-Leitung nicht mehr zur Verf�gung. Um
 *     das Programm neu zu starten muss also die Versorgungsspannung unterbrochen werden.
 *  -# Das Programm l�sst sich nicht im debugging Modus kontrollieren, da die Reset-Leitung
 *     f�r das SpyWire Debug Interface ben�tigt wird. Somit kann die Reset-Leitung w�hrend
 *     des Debugging nicht vom Programm selbst genutzt werden. Das Programm kann nur dann
 *     wie beabsichtigt laufen, wenn der Debug-Modus beendet wird.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3             |
 *       |    |             P1.6|-->LED green
 *      _|_
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Die Steuerregister f�r S2 und S1 werden konfiguriert und anschlie�end der
 *  LPM 0 aktiviert. GIE muss f�r die Funktion des NMI nicht aktiviert werden, wird
 *  aber f�r den Interrupt von S2 ben�tigt.
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */


#include <msp430.h>

int main(void) {
	WDTCTL = WDTPW + WDTHOLD + WDTNMI + WDTNMIES;  	// WDT off NMI hi/lo edge
	P1DIR |= BIT6;                            		// Set P1.6 to output direction
	P1OUT &= ~BIT6;                           		// Clear P1.6 green LED off

	P1REN |= BIT3;									// enable internal resistor
	P1OUT |= BIT3;									// configure internal resistor as pullup resistor

	P1IE |= BIT3;                  					// P1.3 Taster S2: interrupt enabled
	P1IES |= BIT3;                  				// P1.3 Taster S2: interrupt with hi/lo edge
	P1IFG &= ~BIT3;                 				// P1.3 IFG cleared

	IE1 |= NMIIE;                             		// Enable NMI
    __bis_SR_register(GIE);         				// Global interrupt enable
	__bis_SR_register(LPM0_bits);             		// Enter LPM0
}



 /**
  * @brief ISR f�r NMI
  *
  * ISR wird aufgerufen, am an der NMI-Leitung eine fallende Flanke detektiert
  * wird. Die gr�ne LED wird auf on gestellt und ein Wartez�hler gestartet, um
  * den Taster S1 zu entprellen. Anschlie�end muss der NMI erneut aktiviert werden,
  * da dieser bei jedem Interrupt automatisch deaktiviert wird.
  */
#pragma vector=NMI_VECTOR
__interrupt void nmi_(void) {
	volatile unsigned int delay;
	P1OUT |= BIT6;                         			// Set P1.6 green LED on
	for (delay = 20000; delay > 0; delay--)
		;  											// debouncing delay
	IFG1 &= ~NMIIFG;                        		// Reclear NMI flag in case bounce
	IE1 |= NMIIE;                           		// Enable NMI
}

/**
 * @brief ISR f�r Port1
 *
 * ISR wird aufgerufen, sobald an P1.3 eine fallende Flanke auftritt. Mit jedem
 * Aufruf der ISR wird der Wert von cnt inkrementiert. Da die Taste S2 prellt,
 * muss eine Warteschleife zur Entprellung eingesetzt werden.
 */
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	volatile unsigned int delay;
	P1OUT &= ~BIT6;									// Set P1.6 green LED off

	delay = 20000;									// debouncing delay
	while (delay-- > 0);							// wait loop

	P1IFG &= ~BIT3;                 				// P1.3 IFG cleared
}

