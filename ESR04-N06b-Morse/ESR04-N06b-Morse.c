/**
 * @file ESR04-N06b-Morse.c
 * @brief Main File of Project ESR04-N06b-Morse
 * @author S. Steddin
 * @date 02.04.2016
 */

/** \mainpage ESR04-N06b-Morse
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie sich durch das Dr�cken der Taste S2
 *  des MSP430G2 Launchpads die rote LED aktivieren l�sst. Auf diese Weise l�sst
 *  sich das Launchpad als Morse-Signalgeber verwenden.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch Dr�cken der Taste S2 wird die rote LED angeschaltet. Beim Loslassen
 *     der Taste S2 wird die LED wieder ausgeschaltet.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                       MSP430x5xx
 *                     -----------------
 *                 /|\|              XIN|-
 *         int.     | |                 |
 *         pullup   --|RST          XOUT|-
 *            |       |                 |
 *            --------|P1.3         P1.0|-->LED red
 *            |       |                 |
 *        	  \	S2
 *            |
 *           --- GND
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Der Zustand von S2 wird von P1.3 eingelesen. Sobald der Pegel an P1.3 auf L
 *  geht, muss die rote LED angeschaltet, beim Loslassen der LED wieder ausgeschaltet
 *  werden. \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */

#include <msp430.h>

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;			// Stop watchdog timer
	P1DIR |= BIT0;						// Set P1.0 to output direction
										// Any other ports are set to input direction, as all
										// other bit positions are set to 0 by default
	P1REN |= BIT3;						// enable internal Resistor
	P1OUT |= BIT3;						// configure internal Resistor as pullup Resistor

	P1OUT &= ~BIT0;						// disable red LED (redundant: is default state)

	for(;;) {
		if ((P1IN & BIT3) == BIT3) {  	// test S2 is not pressed
			P1OUT &= ~BIT0;				// switch red LED off
		}
		else {
			P1OUT |= BIT0;				// switch red LED on
		}
	}

	return 0;
}


