/**
 * @file main.c
 * @brief Main File of Project ESR-SaveTheBear
 * @author S. Steddin
 * @date 05.05.2018
 * @version 0.1
 */

/** \mainpage ESR06-N01-ToggleRedGreenOnS2ISRwithLPM4
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie eine Applikation schrittweise von
 *  der Idee bis zur fertigen Ausf�hrung entwickelt werden kann. Das Programm
 *  realisiert abschlie�end einen CPR-dummy.
 *
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Die Hardware f�r die Erkennung der Herzdruckmassage und der Beatmung
 *     bestimmungsgem�� mit dem Launchpad verbinden
 *  -# Das Launchpad an die Spannungsversorgung anschlie�en
 *  -# Mit der Reanimation beginnen
 *  -# Mit der Reset-Taste das Programm bei Bedarf neu starten
 *
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Es muss darauf hingewiesen werden, dass bei der Reanimation eines
 *     Menschen andere anatomische Verh�ltnisse vorliegen und eine Reanimation
 *     daher unter anderen Bedigungen erfolgen wird.
 *
 * \subsection remarks Hinweise
 *  -# noch keine Hinweise
 *
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *       S2-->|P1.3         P1.0|-->LED red
 *       |    |             P1.6|-->LED green
 *       |    |                 |      ___
 *      _|_   |             P1.4|-->--|___|--FSR--|---|
 *                      ADC P1.5|<----------------|  _|_
 *
 * .
 * @endcode
 *
 * \subsection versionhistory Versionshistorie
 *  -# Rel. 0.1: \n
       - Dem Top-Down-Design Paradigma entsprechend, wird erst einmal die
         Main-loop mit den erforderlichen Funktionsaufrufen des Top levels
         implementiert. Die Funktionsaufrufe werden als dummys ausgef�hrt.
       - Der Timerinterrupt wird bereits ausgef�hrt, so dass die main-loop
         bereits durchlaufen werden kann.
       - Der Aufruf des ADC-Interrups wird noch nicht ausgef�hrt.
       - Ausgehend von dem hier implementierten Grundger�dt k�nnen nun
         s�mtliche weitere Funktionen iterativ erg�nzt werden.
       - Es dind noch keine Datenstrukturen implementiert.

 * \subsection description Funktionsbeschreibung
 *  Die main Routine wird im 10 ms Takt durchlaufen. Die main Routine ist strikt
 *  nach dem EVA-Prinzip aufgebaut: Zuerst werden Daten eingelesen, dann verarbeitet
 *  und abschlie�end die Daten ausgegeben.
 *

 *  @image html stateDiagram_Main.png "Aufbau des Programms" \n
 *  @image rtf stateDiagram_Main.png width=15cm \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */

#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

void Application_init();
void InputADCData();
void ProcessADCData();
void ProcessCardioSM();
void ProcessRespirSM();
void ProcessBearSM();
void OutputResults();
/*
 *  ======== main ========
 */
int main(void)
{
    Grace_init();                   // Activate Grace-generated configuration
    Application_init();
    
    for (;;) {
    	LPM1;
    	InputADCData();
    	//LPM1;
    	ProcessADCData();
    	ProcessCardioSM();
    	ProcessRespirSM();
    	ProcessBearSM();
    	OutputResults();
    }
    return (0);
}

void Application_init() {

}

/**
 * @brief Aufruf der AD-Wandlung der aktiven Datenkan�le. Es sollen
 *        hierbei 16 samples mit einer Abtastrate von 10 kHz erfasst
 *        werden. Die Wandlung wird ca. 2ms beanspruchen. In dieser
 *        Zeit kann die CPU wieder in den sleep mode gehen.
 */
void InputADCData() {

}

/**
 * @brief Verarbeitung der Rohdaten des ADC in in Format, welches
 *        von den state machines verarbeitet werden kann.
 */
void ProcessADCData() {

}

/**
 * @brief Umsetzung der state machine, welche die Qualit�t der
 *        Herzdruckmassage abbildet.
 */
void ProcessCardioSM() {

}

/**
 * @brief Umsetzung der state machine, welche die Qualit�t der
 *        Beatmung abbildet.
 */
void ProcessRespirSM() {

}

/**
 * @brief Umsetzung der state machine, welche den Fitnesszustand
 *        des B�ren modelliert.
 */
void ProcessBearSM() {

}

/**
 * @brief Ausgabe der ermittelten Daten und Zust�nde an die
 *        weiterverarbeitenden Routinen
 */
void OutputResults() {

}

