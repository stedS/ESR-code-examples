/***************************************************************************************
 *  ESR09-N03-Grace-Tonleiter
 *  ----------------------------------------------------------
 *  Hardware: 		TI launchpad Rev. 1.5 mit MSP430G2553
 *  Software:		Code Composer Studio v6.1
 *  Beschreibung: 	Launchpad soll C-Dur Tonleiter ausgeben: Beginnend beim c bis zum c'
 *  				Das Dr�cken des Schalters S2 soll die Umschaltung auf den n�chst h�heren
 *  				Ton bewirken. Sobald das c' erreicht ist, soll die Tonleiter wieder bei c
 *  				beginnen. S2 muss entprellt sein. Das System wird vom DCO mit 1 MHz getaktet.
 *
 * Beschr�nkungen durch Aufgabenstellung: keine
 *
 *
 *                    MSP4302553
 *                  -----------------
 *                 |                 |
 *            S1-->|RST              |
 *                 |                 |
 *            S2-->|P1.3         P1.2|---|
 *                 |				 |	Lautsprecher
 *  			   |             GND |<--|
 *
 * S2 l�st beim Dr�cken einen Interrupt aus. Innerhalb der Interrupt-Routine
 * wird die n�chste Tonfrequenz aktiviert. Die zu den Tonfrequenzen geh�renden
 * Vergleichswerte von Compare Register 1 von TimerA3_0 werden vorberechnet
 * und in einer Lookup-Table abgelegt.
 *
 * Weitere Besonderheit: die in der ISR von Taste S2 auszuf�hrenden Programmschritte
 * werden in einer Funktion in main.c behandelt. Auf diese Weise bleibt der von Grace
 * erzeugte Code von InterruptVectors.c unver�ndert und die selbst erstellten
 * Programmfunktionen finden sich an einer Stelle (main.c)
 *
 * Verfahren l�uft ohne CPU: Low Power Mode 1
 *
 *  Prof. Dr. S. Steddin
 *  HS-Reutlingen - Med. Technische Informatik
 *  2014-05-04
 */
#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

//Umrechnung der Tonleiterfrequenzen in Timerz�hlerst�nde,
//ausgehend von a' (440Hz):
// c'' = h' * 2^(1/12) --> 1910
// h'  = a' * 2^(2/12) --> 2024
// a'  = 440 Hz        --> 2272
// g'  = a' / 2^(2/12) --> 2550
// f'  = g' / 2^(2/12) --> 2862
// e'  = f' / 2^(1/12) --> 3032
// d'  = e' / 2^(2/12) --> 3404
// c'  = d' / 2^(2/12) --> 3821
//
//Beachten: die Periode (Z�hlerwert) ist reziprok zur Frequenz!


//									c', ...                                     c''
const unsigned int cTonleiter[] = { 3821, 3404, 3032, 2862, 2550, 2272, 2024, 1910};
//Index f�r Zugriff auf Frequenztabelle:
unsigned int uiTonIdx = 0;

/*
 *  ======== main ========
 */
int main(void)
{
    Grace_init();                   // Activate Grace-generated configuration

    // >>>>> Fill-in user code here <<<<<

    return (0);
}



void S2_ISR(void)
{
	volatile unsigned int ii = 1000;

	//Entprellen der Taste S2:
	//a) Warten bis die Taste sich beruhigt hat:
	while (ii--)
		_nop();

	//b) Nur dann reagieren, wenn die Taste gedr�ckt ist (L-Pegel):
	if (!(P1IN & BIT3))
	{
		//N�chsten Ton selektieren:
		uiTonIdx++;
		if (uiTonIdx >= 8)
			uiTonIdx = 0;

		TA0CCR0 = cTonleiter[uiTonIdx];
		TA0CCR1 = TA0CCR0 >> 1;	//Tastverh�ltnis 50%
	}

	//Interruptflag zur�ck setzen:
	P1IFG &= ~BIT3;
}
