/**
 * @file ESR05-N03-ToggleRedGreenOnS2ByISR-debounced.c
 * @brief Main File of Project ESR05-N03-ToggleRedGreenOnS2ByISR-debounced
 * @author S. Steddin
 * @date 18.04.2018
 */

/** \mainpage ESR05-N03-ToggleRedGreenOnS2ByISR-debounced
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie die Ausf�hrung einer bestimmten
 *  Aufgabe innerhalb einer ISR erfolgen kann. Die Aktivierung der ISR
 *  wird durch einen Tastendruck ausgel�st. Es soll das Tastenprellen erkannt
 *  werden, indem ein Z�hler die vom MC registrierten ISR Aufrufe mitz�hlt.
 *  Durch geeignete Ma�nahmen soll der Taste "entprellt" werden (debouncing)
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Die Taste S2 wird z.B. 10 mal gedr�ckt. Anschlie�end wird in der ISR
 *     ein Breakpoint gesetzt, so dass beim n�chsten Tastendruck der Debugger
 *     in der ISR stehenbleibt. Nun kann der Wert des ISR-Z�hlers ausgelesen werden.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                       MSP430x5xx
 *                     -----------------
 *                 /|\|              XIN|-
 *         int.     | |                 |
 *         pullup   --|RST          XOUT|-
 *            |       |                 |
 *            --------|P1.3         P1.0|-->LED red
 *            |       |             P1.6|-->LED green
 *        	  \	S2
 *            |
 *           --- GND
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Durch Aktivierung des P1-Portinterrupts wird das Dr�cken der Taste S2
 *  �ber den Porteingang P1.3 �berwacht. Sobald die Taste gedr�ckt wird, wird
 *  die zugeh�rige Interruptroutine aufgerufen. Innerhalb der Interruptroutine
 *  wird eine Z�hlervariable inkrementiert. Die R�ckkehr aus der ISR erfolgt erst
 *  nach dem Durchlauf einer Warteschleife.\n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */


#include <msp430.h>

/**
 * @brief Das Hauptprogramm konfiguriert die erforderlichen Register
 *
 * Sobald die Konfiguration abgeschlossen ist, geht das Hauptprogramm in
 * eine Endlosschleife und wartet darauf, von einer Interruptroutine
 * unterbrochen zu werden.
 */
int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= BIT0 + BIT6;			// Set P1.0 and P1.6 to output direction
									// Any other ports are set to input direction, as all
									// other bit positions are set to 0 by default
	P1REN |= BIT3;					// enable internal Resistor
	P1OUT |= BIT3;					// configure internal Resistor as pullup Resistor

	P1IE |=  BIT3;                  // P1.3 Taster S2: interrupt enabled
	P1IES |= BIT3;                  // P1.3 Taster S2: interrupt with hi/lo edge
	P1IFG &= ~BIT3;                 // P1.3 IFG cleared

	P1OUT |= BIT0;					// enable red LED
	P1OUT &= ~BIT6;					// disable green LED

    __bis_SR_register(GIE);         // Global interrupt enable

	while (1)
		;
	return 0;

}


/**
 * @brief ISR f�r Port1
 *
 * ISR wird aufgerufen, sobald an P1.3 eine fallende Flanke auftritt. Mit jedem
 * Aufruf der ISR wird der Wert von cnt inkrementiert. Da die Taste S2 prellt,
 * muss eine Warteschleife zur Entprellung eingesetzt werden.
 */
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	static unsigned int cnt = 0;
	volatile unsigned int ii;
	cnt++;
	P1OUT ^= BIT0 + BIT6;					// toggle green and red LED

	ii = 40000;								// Wartez�hler f�r Entprellung
	while (ii-- > 0);

	P1IFG &= ~BIT3;                         // P1.3 IFG cleared
}


