/**
 * @file blink.c
 * @brief Main File of Project ESR02-A01-Blinkenlights01
 * @author S. Steddin
 * @date 08.04.2018
 */

/** \mainpage ESR02-A01-Blinkenlights01
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie die rote LED des MSP430G2 Launchpads
 *  zum Blinken gebracht werden kann. Die Dauer der Blinkperiode wird �ber eine
 *  Z�hlerschleife festgelegt. Im Programm soll empirisch ermittelt werden, wie viele
 *  Schleifendurchl�ufe erforerlich sind, um eine Blinkfrequenz von 1 Hz zu erhalten.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Durch �ndern der Z�hlervariable i mit Hilfe einer Stoppuhr ermitteln,
 *     wie lange es dauert, bis die LED an- oder ausgeht. Auf diese Weise l�sst
 *     sich die Zeit absch�tzen, die f�r einen Schleifendurchlauf erforderlich ist.
 *  -# Die Applikation l�uft, ohne durch Eingaben beeinflusst werden zu k�nnen.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Die Z�hlervariable als volatile deklarieren, wenn diese f�r das Debugging
 *     ben�tigt wird.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                MSP430x5xx
 *             -----------------
 *         /|\|              XIN|-
 *          | |                 |
 *          --|RST          XOUT|-
 *            |                 |
 *            |             P1.0|-->LED
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Toggle P1.0 by xor'ing P1.0 inside of a software loop. \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1  */

#include <msp430.h>				

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;					// Set P1.0 to output direction

	for(;;) {
		volatile unsigned int i;	// volatile to prevent optimization

		P1OUT ^= 0x01;				// Toggle P1.0 using exclusive-OR

		//i = 1000000;				// SW Delay : Test for long period, which can
									// be measured with stop watch. Result: approx. 15s
									// 0,5s : required cycles: 1000000 / 15s * 0,5s =
									// 33333
		i = 33333;
		do i--;
		while(i != 0);
	}
	
	return 0;
}
