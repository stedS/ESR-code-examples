/**
 * @file ESR10-N01-Grace-DaeSchalter.c
 * @brief Main file of Project ESR10-N01-Grace-DaeSchalter
 * @author Prof. Dr. S. Steddin, Reutlingen University
 * @date 16.05.2018
 */

/** \mainpage
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie der Comparator Eingang verwendet werden
 *  kann, um beim �ber- oder Unterschreiten einer Spannung einen Interrupt auszul�sen.
 *  Die zu �berwachende Spannung wird �ber einen lichtempfindlichen Widerstand gesteuert.
 *  Beim Unterschreiten einer bestimmten Beleuchtungsst�rke am LDR-Sensor soll
 *  die rote LED an- und die gr�ne LED ausgeschaltet werden. Beim �berschreiten einer
 *  bestimmten Beleuchtungsst�rke soll die rote LED aus- und die gr�ne LED angeschaltet
 *  werden.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Den LDR mit Eingang P1.4 verbinden.
 *  -# Den LDR abwechselnd mit der Hand vom Licht abdecken oder der Lichteinstrahlung
 *     freigeben (Widerstand sinkt).
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# keine
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *
 *                    MSP4302553
 *                  -----------------
 *                 |         Vcc     |
 *    rote LED <---|P1.0       |     |
 *                 |          | |    |
 *   gr�ne LED <---|P1.6       | P1.4|---|   Comp: neg. Eingang
 *                 |				 |	LDR
 *    Comp Out <---|P1.7         GND |<--|
 *                 |                 |
 *
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 * Der Widerstand eines LDR steigt an, wenn die Helligkeit abnimmt.
 * Der LDR wird �ber P1.4 mit dem neg. Eingang des Comparator A Moduls
 * verbunden. F�r P1.4 wird gleichzeitig der interne Pullup-Widerstand aktiviert,
 * so dass der Pullup-Widerstand und der LDR einen Spannungsteiler bilden.
 *
 * - Unterschreitet die Helligkeit eine bestimmte Beleuchtungsst�rke, so �bersteigt die
 *   am Minus-Eingang des Comp. liegende Spannung den Spannungswert der am Plus-Eingang
 *   liegenden internen Referenzspannung --> der Ausgang des Comp. geht auf L
 *
 * - �berschreitet die Helligkeit eine bestimmte Beleuchtungsst�rke, so f�llt
 *   die am Minus-Eingang des Comp. liegende Spannung unter den am Plus-Eingang liegenden
 *   Vergleichswert --> der Ausgang des Comp. geht auf H
 *
 * - Der Pegelwechsel am Comparatorausgang soll einen Interrupt ausl�sen. Innerhalb der
 *   Interrupt Routine wird jeweils eingestellt, auf welche Flanke (fallend oder steigend)
 *   der n�chste Interrupt erfolgen soll und wie hoch der am Plus-Eingang des Comp. liegende
 *   Vergleichswert sein soll, damit beim Schalten des Comparators eine Hysterese auftritt.
 *   Au�erdem werden die LEDs entsprechend der oben beschriebenen Anforderung eingestellt.
 * \n
 * Weitere Besonderheit: die in der ISR des Comparators auszuf�hrenden Programmschritte
 * werden in einer Funktion in main.c behandelt. Auf diese Weise finden alle �nderungen
 * an der Programmlogik in main.c statt. In der von Grace erzeugte Code Datei InterruptVectors.c
 * muss nur der Aufruf der in main.c enthaltenen Funktion Comp_ISR() aufgerufen werden.
 * \n
 * Verfahren l�uft ohne CPU: Low Power Mode 1 *
 *
 *  @image html stateDiagram.png "Aufbau des Programms" \n
 *  @image rtf stateDiagram.png width=15cm \n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */



/*
 * ======== Standard MSP430 includes ========
 */
#include <msp430.h>

/*
 * ======== Grace related includes ========
 */
#include <ti/mcu/msp430/Grace.h>

/*
 *  ======== main ========
 */
int main(void)
{
    Grace_init();                   // Activate Grace-generated configuration
    LPM0;
    
    // >>>>> Fill-in user code here <<<<<
    
    return (0);
}


void Comp_ISR()
{
	//beachten:
	//	LDR ist am - - Eingang angeschlossen
	//	Vergleichsspannung Ref am + -Eingang
	//--> Comp Ausgang dann 1 wenn Ref > U_LDR
	//  Grundeigenschaft des LDR: je mehr Licht, desto kleiner der Widerstand


	CACTL1 &= ~CAIE;	//Komparator-Interrupt deaktivieren:
						//... nicht zwingend erforderlich, da  beim Eintritt in die
						//ISR das  GIE-Flag sowieso gel�scht wird. Befehl wird
						//trotzdem aufgerufen, um das erwartete Verhalten zu
						//dokumentieren.

    //Pr�fen in welchem Zustand sich der Comparatorausgang nach dem Ausl�sen des
	//Interrupts befindet:
	if ((CACTL2 & BIT0) == BIT0)	//wenn der Ausgang auf H steht -->
    {								//steigende Flanke muss Interrupt ausgel�st haben:

    	//rising edge: Sensor erhellt : steigende Flanke am Comparatorausgang
    	//l�ste Interrupt aus, d.h. U_LDR (- Eingang) fiel unter die Spannung
    	//U_REF (+ Eingang).
    	//wenn hell: gr�ne LED an, rote LED aus

    	P1OUT &= ~BIT0;		//rote LED aus
    	P1OUT |= BIT6;		//gr�ne LED an

    	//N�chstes Ereignis: Sensor verdunkelt : fallende Flanke am Comparatorausgang
    	//soll Interrupt ausl�sen, d.h. U_LDR soll wieder �ber U_REF steigen
    	CACTL1 |= CAIES;	//n�chster Interrupt �ber fallende Flanke am Comparator Ausgang

    						//Hysterese: nach dem Unterschreiten von 0,25 Vcc muss die
    						//LDR Spannung nun �ber 0,5 Vcc steigen(Verdunkelung), damit
    						//Comparator Ausgang von H auf L wechselt und die LEDs wieder
    						//umgeschaltet werden:
		CACTL1 &= ~(CAREF_1 | CAREF_2);	//  - die eben unterschrittene Schwelle wird gel�scht (0,25 Vcc)
		CACTL1 |= CAREF_2;				//  - die zu �berschreitende Schwelle wird gesetzt (0.5 Vcc)
    }
    else
    {
    	//falling edge: Sensor abgedunkelt
    	P1OUT |= BIT0;		//rote LED an
    	P1OUT &= ~BIT6;		//gr�ne LED aus
    	CACTL1 &= ~CAIES;	//n�chster Interrupt �ber steigende Flanke am Comparator Ausgang

    						//Hysterese: nach dem �berschreiten von 0,5 Vcc muss die
							//LDR Spannung unter 0,25 Vcc (Erhellung) fallen, damit LEDs umschalten
    	CACTL1 &= ~(CAREF_1 | CAREF_2);	//  - die �berschrittende Schwelle wird gel�scht (0,5 Vcc)
		CACTL1 |= CAREF_1;				//  - die zu unterschreitende schreitende Schwelle wird gesetzt (0.25 Vcc)
    }
	// family user handbook:
	// "... The CAIFG flag is automatically reset when the interrupt request is serviced or may be reset with
	//  software." D.h., das Interrupt Flag sollte bereits gel�scht sein. Es wird an dieser Stelle trotzdem
	//  gel�scht, um bei sp�terem Lesen des Programmes zu erkennen, was passiert ohne das implizite Verhalten
	//  des Prozessors kennen zu m�ssen.
	CACTL1 &= ~CAIFG;   //Interruptflag l�schen

	//... siehe Kommentar am Beginn der Routine:
	CACTL1 |= CAIE;		//Komparator-Interrupt wieder aktivieren
}
