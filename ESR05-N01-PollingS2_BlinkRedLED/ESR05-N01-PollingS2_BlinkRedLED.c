/**
 * @file ESR05-N01-PollingS2_BlinkRedLED.c
 * @brief Main File of Project ESR05-N01-PollingS2_BlinkRedLED
 * @author S. Steddin
 * @date 18.04.2018
 */

/** \mainpage ESR05-N01-PollingS2_BlinkRedLED
 *
 * \section intro_sec Einleitung
 * \subsection purpose Zweckbestimmung
 *  Das Projekt dient dazu zu zeigen, wie, abh�ngig von der Aktivierung
 *  der Taste S2 eine Subfunktion aufgerufen wird.
 * \subsection intended_use Bestimmungsgem��er Gebrauch
 *  -# Das MSP430 Launchpad an die Versorgungsspannung anschlie�en.
 *  -# Solange die Taste S2 gedr�ckt bleibt blinkt die rote LED mit einer
 *     �ber eine Z�hlerschleife vorgegebenen Frequenz.
 * \subsection forseeable_misuse Vorhersehbare fehlerhafte Anwendung
 *  -# Aktuell keine risikobehaftete Fehlanwendung erkennbar
 * \subsection remarks Hinweise
 *  -# Da an S2 kein externer Pullup-Widerstand angeschlossen ist, muss der
 *     interne Pullup-Widerstand von P1.3 aktiviert werden
 *  -# Ist die Dauer der Z�hlerschleife zu gering, dann wechselt die LED so
 *     schnell ihren Zustand, dass dies f�r das menschliche Auge nicht mehr
 *     wahrnehmbar ist.
 * \subsection pin_configuration Anschlussbelegung
 * @code
 * .
 *                       MSP430x5xx
 *                     -----------------
 *                 /|\|              XIN|-
 *         int.     | |                 |
 *         pullup   --|RST          XOUT|-
 *            |       |                 |
 *            --------|P1.3         P1.0|-->LED red
 *            |       |                 |
 *        	  \	S2
 *            |
 *           --- GND
 * .
 * @endcode
 *
 * \subsection description Funktionsbeschreibung
 *  Der Zustand von S2 wird von P1.3 eingelesen. Sobald der Pegel an P1.3 auf L
 *  geht, muss die rote LED, abh�ngig vom Stand eines Z�hlers, wechselweise an-
 *  und ausgeschaltet, so dass die LED blinkt.\n
 *  ACLK = n/a, MCLK = SMCLK = default DCO \n
 * \subsection Hardware Hardware
 *  - Microcontroller MSP430G2553
 *  - MSP430G2 Launchpad, Rev. 1.5
 * \subsection Software Software
 *  - Built with Code Composer Studio 6.1.1
 */
#include <msp430.h>
#include <intrinsics.h>
#include <stdint.h>
#include <stdbool.h>

void LED_blinken();				//forward declaration

/**
 * @brief Aufruf der Subfunktion LED_blinken() sobald die Taste S2 gedr�ckt wird.
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	P1REN |= BIT3;				// enable internal Resistor
	P1OUT |= BIT3;				// configure internal Resistor as pullup Resistor
	P1DIR |= BIT0;				// red LED on P1.0 as output
	P1OUT &= ~BIT0;				// switch off red LED


    for(;;) {
    	if ((P1IN & BIT3)==BIT3) {		//test S2: do nothing if S2 not pressed
    		_nop();
    	}
    	else {
    		LED_blinken();				//call subroutine as soon as S2 pressed
    	}
    }
	return 0;
}

/**
 * @brief Rote LED blinken lassen, bis Taste S2 wieder losgelassen wird.
 *
 * Beim Verlassen der Funktion wird die LED ausgeschaltet.
 */
void LED_blinken(void) {
	unsigned int cnt = 0;
	while (!((P1IN & BIT3)==BIT3)) {
		if (cnt++ <= 0x7FFF) {
			P1OUT |= BIT0;
		}
		else {
			P1OUT &= ~BIT0;
		}
	}
	P1OUT &= ~BIT0;
}
